// ==UserScript==
// @name         Get 5e.tools Permalink
// @namespace    https://gitlab.com/binarmorker/5e-tools-permalink
// @version      1.0.0
// @description  Add Permalink button under supported items in 5e.tools
// @author       François Allard
// @match        *://5e.tools/bestiary.html
// @match        *://5e.tools/items.html
// @match        *://5e.tools/spells.html
// @match        *://*.5e.tools/bestiary.html
// @match        *://*.5e.tools/items.html
// @match        *://*.5e.tools/spells.html
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    function loadButton() {
        var container = document.querySelector('#contentwrapper .wrp-footer-buttons');

        if (container) {
            var origin = location.origin;
            var url = location.href.substring(origin.length)
                .replace('.html#', '/')
                .replaceAll(/[\-',\.\(\)\*]/g, '')
                .replaceAll('%20', '-')
                .replaceAll('%2b', '')
                .replaceAll('%2c', '')
                .replaceAll('%2f', '')
                .replaceAll('_', '-')
                .replaceAll('%c3%97-', '')
            + '.html';
            var existingButton = container.querySelector('#permalink-button');

            if (existingButton) {
                existingButton.href = url;
            } else {
                var button = document.createElement('a');
                button.appendChild(document.createTextNode('Get permalink'));
                button.href = url;
                button.id = 'permalink-button';
                button.classList = 'btn btn-xs btn-warning';
                container.appendChild(button);
            }
        }
    }

    loadButton();
    window.addEventListener('hashchange', loadButton);
})();